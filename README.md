 # Exploring 8a.nu Data

 This repository is an interactive introduction to data visualization
 using Python, Jupyter, Pandas and Bokeh.

 ## Requirements

 * Python 3.6
 * [Pipenv](https://docs.pipenv.org/)
 * (Recommended) [pyenv](https://github.com/pyenv/pyenv)

 ## Setup

To install Python 3 and Pipenv on macOS:

```sh
# install Python, Pipenv and pyenv
brew install python3 pipenv pyenv
```

`python3` may install as Python 3.7 if you are installing it for the first time. This project requires
Python 3.6, but Pipenv can automatically download the correct version of Python if `pyenv` is installed.

With the repository cloned:

```sh
# install Python dependencies
pipenv install
# start the Jupyter server
pipenv run jupyter notebook
```

## Sources and Links

* [8a.nu data set on Kaggle](https://www.kaggle.com/dcohen21/8anu-climbing-logbook) (thanks David Cohen)
* [Geo-Maps](https://github.com/simonepri/geo-maps) (thanks @simonepri)
* [Python](https://www.python.org/)
* [Jupyter Project](http://jupyter.org/)
* [Pandas](http://pandas.pydata.org/)
* [Bokeh](https://bokeh.pydata.org/en/latest/)
